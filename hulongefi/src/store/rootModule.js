import { getSlider } from '../api/public.js';
import * as types from './action-types.js';//存放所有方法的类型，为了直观看到哪些方法调用
import WS from '@/utils/websocket'
//console.log(types)
export default {
    state: {
        sliders: [],
        ws: null,
        message: ''
    },
    mutations: {
        [types.SET_SLIDERS](state, payload) {
            state.sliders = payload;
        },
        [types.CREATE_WEBSOCKET](state, ws) {//存放ws信息
            state.ws = ws
        },
        [types.SET_MESSAGE](state, msg) {//存放msg信息
            state.message = msg
        }
    },
    actions: {
        async [types.SET_SLIDERS]({ commit }) {
            let { data } = await getSlider();
            commit(types.SET_SLIDERS, data)
        },
        async [types.CREATE_WEBSOCKET]({ commit }) {
            let ws = new WS();
            ws.create();//调用src/utils/websocket.js中create的方法
            commit(types.CREATE_WEBSOCKET, ws);
        }
    }
}
