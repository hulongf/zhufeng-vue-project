import config from './config/puclic.js';
import axios from '@/utils/request.js'
import { getLocal } from '@/utils/local.js' //新建local.js文件存储本地会话
// 获取轮播图功能
export const getSlider = () => axios.get(config.getSlider);
export const getCaptcha = () => axios.get(config.getCaptcha, {
      params: {
          uid: getLocal('uuid')
      }
  })